#include "BadCaseBuilder.h"
#include "gurobi_c++.h"
#include <cassert>
#include <map>
#include <set>
#include "Timer.h"
#include "GRBEnvSingleton.h"
#include "globals.h"

using namespace std;

std::vector<std::vector<int> > BadCaseBuilder::normalizationConstraints;
std::vector<std::tuple<int, BadCaseBuilder::Op, int> > BadCaseBuilder::additionalConstraints;

BadCaseBuilder::BadCaseBuilder()
{
}

BadCaseBuilder::~BadCaseBuilder()
{
}

void BadCaseBuilder::addCritPathes(std::vector<std::vector<int>> m)
{
	maximize = m;
}

void BadCaseBuilder::generateStaticConstrains(Graph &g, std::istream & inp)
{
	assert(normalizationConstraints.size() == 0);
	set<int> masks;
	vector<Node*> nodes = g.get_nodes();
	for (int i = 0; i < nodes.size(); i++) {
		masks.insert(nodes[i]->get_job_mask());
		
	}

	assert(masks.size() > 0);
	for (auto rit = masks.rbegin(); rit != masks.rend(); rit++) {
		int mask = *rit;
		for (int j = 0; (mask >> j) > 0; j++) {
			if ((mask >> j) % 2 == 1) {
				if (normalizationConstraints.size() < j + 1)
					normalizationConstraints.resize(j + 1);
				normalizationConstraints[j].push_back(mask);
			}
		}
	}

	// Read additional constraints
	int n = 0;
	if (!(inp >> n)) {	// EOF. No additional constraints in a file.
		return;
	}

	for (int i = 0; i < n; i++) {
		int a, b;
		Op op = Initial;
		string buf;
		inp >> buf;
		assert(buf.length() > 0);
		a = std::stoi(buf);

		inp >> buf;
		if (buf[0] == '>') op = More;
		else if (buf[0] == '<') op = Less;
		assert(buf[1] == '=');
		inp >> buf;
		b = std::stoi(buf);
		additionalConstraints.push_back(std::make_tuple(a, op, b));
		//cout << a << ' ' << (const char*)(op==More? ">=" : "<=") << ' ' << b << endl;
	}
}

/*
void BadCaseBuilder::generateConstrains(int machine_cnt)
{
	assert (normalizationConstraints.size() == 0) ;

	normalizationConstraints.resize(machine_cnt);

	for (int i = 1; i < (1 << machine_cnt) - 1; i++)
		for (int j = 0; (i >> j) > 0; j++)
			if ((i >> j) % 2 == 1)
				normalizationConstraints[j].push_back(i);
}
*/

Case BadCaseBuilder::buildCase(ostream & cerr)
{
	assert(maximize.size() > 0);
	assert(normalizationConstraints.size() > 0);

	GRBModel model(GRBEnvSingleton::get());
	if (!Args::VerobseGurobi) {
		model.set(GRB_IntParam_LogToConsole, 0);
	}

	map<int, GRBVar> jobs;
	for (int i = 0; i < normalizationConstraints.size(); i++) {
		GRBLinExpr c = 0.0;
		for (int j = 0; j < normalizationConstraints[i].size(); j++) {
			if (jobs.count(normalizationConstraints[i][j]) == 0) jobs[normalizationConstraints[i][j]] = model.addVar(0.0, 10.0, 0.0, GRB_CONTINUOUS, string("j") + to_string(normalizationConstraints[i][j]));
			GRBVar & lst = jobs[normalizationConstraints[i][j]];
			c += lst;
		}
		model.addConstr(c <= 1, to_string(i) + "th_constrain");
	}

	for (auto it = additionalConstraints.begin(); it != additionalConstraints.end(); it++) {
		auto tpl = *it;
		GRBVar a = jobs[std::get<0>(tpl)];
		Op op = std::get<1>(tpl);
		GRBVar b = jobs[std::get<2>(tpl)];
		assert(op != Initial);
		if (op == More) {
			model.addConstr(a >= b, "todo");
		}
		else {
			assert(op == Less);
			model.addConstr(a <= b, "todo");
		}
	}

	GRBVar lambda = model.addVar(0.0, 100.0, 0.0, GRB_CONTINUOUS, string("lambda"));
	for (int i = 0; i < maximize.size(); i++) {
		GRBLinExpr c = 0.0;
		for (int j = 0; j < maximize[i].size(); j++) {
			if (jobs.count(maximize[i][j]) == 0) jobs[maximize[i][j]] = model.addVar(0.0, 10.0, 0.0, GRB_CONTINUOUS, string("j") + to_string(maximize[i][j]));
			GRBVar & lst = jobs[maximize[i][j]];
			c += lst;
		}
		model.addConstr(c >= lambda, to_string(i) + "th_maximize");
	}

	model.setObjective(lambda + 0, GRB_MAXIMIZE);
	//model.write("debug.lp");
	Timer::start(Timer::LP_BAD_CASE_BUILDING);
	model.optimize();
	Timer::stop(Timer::LP_BAD_CASE_BUILDING);
	//cerr << "Lambda: " << lambda.get(GRB_DoubleAttr_X) << endl;
	this->lambda = lambda.get(GRB_DoubleAttr_X);

	Case bad_case;
	for (auto it = jobs.begin(); it != jobs.end(); it++)
		bad_case.add_job(Job(it->first, it->second.get(GRB_DoubleAttr_X)));
	return bad_case;
}

double BadCaseBuilder::get_lambda()
{
	return this->lambda;
}
