#pragma once
#include<vector>
//#include<iostream>
#include<string>
#include "Case.h"
#include "Graph.h"
#include <tuple>

// For now job numbering convention is job_number=bitmask_of_machines_it_present_on.

class BadCaseBuilder
{
private:
	enum Op {
		Initial,
		More,
		Less
	};

	std::vector<std::vector<int> > maximize;

	static std::vector<std::vector<int> > normalizationConstraints;			// Used to make load on machine <= 1
	static std::vector<std::tuple<int, Op, int> > additionalConstraints;

	double lambda;

public:
	BadCaseBuilder();
	~BadCaseBuilder();

	void addCritPathes(std::vector<std::vector<int> > m);
	static void generateStaticConstrains(Graph &g, std::istream & inp);
//	void generateConstrains(int machine_cnt); never used
	Case buildCase(std::ostream & cerr);
	double get_lambda();

	void write_model();
};
