#include "Case.h"

using namespace std;

Case::Case()
{
}


Case::~Case()
{
}

Job Case::get_job(int idx)
{
	return jobs[idx];
}

int Case::get_job_cnt()
{
	return (int)jobs.size();
}

void Case::add_job(Job j)
{
	jobs.push_back(j);
}

Job::Job(int m, double d)
{
	this->set_machines(m);
	this->set_duration(d);
}

double Job::get_duration()
{
	return duration;
}

int Job::get_machines()
{
	return machines;
}

void Job::set_duration(double d)
{
	this->duration = d;
}

void Job::set_machines(int m)
{
	this->machines = m;
}

vector<int> Job::machines_as_array()
{
	vector<int> m;
	for (int j = 0; (machines >> j) > 0; j++)
		if ((machines >> j) % 2 == 1) m.push_back(j+1);
	return m;
}

int count_ones(int val)
{
	int cnt = 0;
	while (val != 0) {
		cnt++;
		val = val & (val - 1);
	}

	return cnt;
}

void Job::print(std::ostream &out)
{
	/*
	out << duration << ' ' << count_ones(machines) << ' ';
	for (int i = 0; (machines >> i) > 0; i++) {
		if ((machines >> i) % 2 == 1)
			out << i + 1 << " ";
	}
	out << endl;
	*/
	out << duration << ' ' << machines << endl;
}

void Case::print(ostream &out)
{
	out << this->get_job_cnt() << endl;
	for (int i = 0; i < this->get_job_cnt(); i++) {
		jobs[i].print(out);
	}
}
