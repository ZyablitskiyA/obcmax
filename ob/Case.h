#pragma once
#include <iostream>
#include <vector>
#include <string>

class Job {
protected:
	double duration;
	int machines;
public:
	Job(int m, double d);
	double get_duration();
	int get_machines();

	void set_duration(double d);
	void set_machines(int m);
	std::vector<int> machines_as_array();
	void print(std::ostream &);
};

class Case
{
protected:
	std::vector<Job> jobs;
public:
	Case();
	~Case();

	Job get_job(int idx);
	int get_job_cnt();
	void add_job(Job j);
	void print(std::ostream &);
};
