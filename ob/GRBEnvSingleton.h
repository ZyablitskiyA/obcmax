#pragma once
#include "gurobi_c++.h"

// By manual GRBEnv is very expensive to create. It is recomended to have one GRBEnv per thread. We are single threaded.
class GRBEnvSingleton
{
private:
	static GRBEnv grbenv;
public:
	static const GRBEnv& get() { return grbenv; }
};
