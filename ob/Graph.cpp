#include "Graph.h"
#include "Case.h"
#include <iostream>
#include <cassert>
#include <algorithm>

using namespace std;

Graph::Graph(bool leased)
{
	_leased = leased;
}

// Not tested at all
Graph::Graph(std::istream & inp, bool leased) : Graph(leased)
{
	int v_num;
	inp >> v_num;
	for (int i = 0; i < v_num; i++) {
		char trash;
		int job_mask, machine_mask;
		inp >> job_mask >> trash >> machine_mask;
		int e_num;
		inp >> e_num;
		Node *cur = find_or_create_node(job_mask, machine_mask);
		for (int j = 0; j < e_num; j++) {
			inp >> job_mask >> trash >> machine_mask;
			Node *nxt = find_or_create_node(job_mask, machine_mask);
			cur->add_edge(nxt);
		}
	}
	// Check operations of the same job are well connected
	// Check graph for being acyclic and for transitive edges
}

void Graph::add_vertex(Node * ptr)
{
	nodes.push_back(ptr);
}

void Graph::set_visited_to(int val)
{
	for (int i = 0; i < nodes.size(); i++)
		nodes[i]->visited = val;
}

Node * Graph::find_or_create_node(int job_mask, int machine_mask)
{
	for (int i = 0; i < nodes.size(); i++)
		if (nodes[i]->get_job_mask() == job_mask && nodes[i]->get_machine_mask() == machine_mask)
			return nodes[i];

	nodes.push_back(new Node(job_mask, machine_mask));
	return nodes.back();
}

void Graph::dfs_ts(Node *cur, vector<Node*> &topsort)
{
	if (cur->visited == 1) throw new exception("This is not a DAG. Loop found.");
	if (cur->visited == 2) return;

	cur->visited = 1;
	for (int i = 0; i < cur->nxt.size(); i++) {
		dfs_ts(cur->nxt[i], topsort);
	}

	cur->visited = 2;
	topsort.push_back(cur);
}

// TODO: improve performance by topological sorting initial array and maybe freezing it then and memorizing it is top sorted.
std::vector<Node*> Graph::top_sort()
{
	set_visited_to(0);

	vector<Node*> topsort;
	for (int i = 0; i < nodes.size(); i++)
		dfs_ts(nodes[i], topsort);
	std::reverse(topsort.begin(), topsort.end());

	return topsort;
}

std::vector<Node*> Graph::get_nodes()
{
	return nodes;
}

// Needed for transitive path removal.
void Graph::dfs(Node* cur) {
	if (cur == nullptr)						// Should never happen. Possible if we have a cycle.
		return;
	if (cur->visited == 2)
		return;
	assert(cur->visited == 0);					// No cycles
	cur->visited = 1;
	int n = cur->get_next_cnt();
	for (int i = 0; i < n; i++) {
		dfs(cur->get_next(i));
	}

	cur->visited = 2;
}

void Graph::remove_transitive_edges()
{
	for (int i = 0; i < (int)nodes.size(); i++) {
		Node *cur = nodes[i];
		for (int j = 0; j < cur->get_next_cnt(); j++) {
			Node *nxt = cur->get_next(j);
			if (nxt == nullptr)							// Skipping already removed edge.
				continue;
			set_visited_to(0);
			dfs(nxt);
			for (int k = 0; k < cur->get_next_cnt(); k++) {
				Node *nxt_remove_candidate = cur->get_next(k);
				if (nxt_remove_candidate == nullptr)
					continue;
				if (nxt_remove_candidate == nxt)
					continue;
				if (nxt_remove_candidate->visited > 0)
					cur->nxt[k] = nullptr;				// Got to dfs-visited node not through cur -> nxt edge, zeroing transitive edge.
			}
		}
		cur->nxt.erase(std::remove(cur->nxt.begin(), cur->nxt.end(), nullptr), cur->nxt.end());			// actually remove all nullptr's from the nxt vector in a single pass.
	}
}

void Graph::draw(std::ostream &out)
{
	out << nodes.size() << endl;
	for (int i = 0; i < nodes.size(); i++) {
		out << nodes[i]->get_job_mask() << ":" << nodes[i]->get_machine_mask() << " " << nodes[i]->get_next_cnt() << " ";
		for (int j = 0; j < nodes[i]->get_next_cnt(); j++) {
			Node *nxt = nodes[i]->get_next(j);
			out << nxt->get_job_mask() << ":" << nxt->get_machine_mask() << " ";
		}
		out << std::endl;
	}
}

// This is a dfs written on explicit stack.
// TODO: remove trivial pathes.
std::vector<std::vector<int>> Graph::list_complete_paths()
{
	// Here field visited is used as node indegree.
	set_visited_to(0);
	for (int i = 0; i < nodes.size(); i++)
		for (int j = 0; j < nodes[i]->nxt.size(); j++) {
			nodes[i]->nxt[j]->visited++;
		}

	vector<vector<int> > path;

	for (int i = 0; i < nodes.size(); i++) {
		if (nodes[i]->visited == 0) {										// i - start point of complete path(es)
			vector<Node*> u_s;
			vector<int> i_s, jmask_s;										// jmask_s - stack of job masks of grpah nodes for current dfs state
			vector<int> is_trivial_s;
			jmask_s.push_back(nodes[i]->get_job_mask());
			u_s.push_back(nodes[i]);
			i_s.push_back(0);
			is_trivial_s.push_back(u_s.back()->get_machine_mask());
			while (!u_s.empty()) {
			outer_continue:;
				if (u_s.back()->nxt.size() == 0 && !is_trivial_s.back()) path.push_back(jmask_s);	// save complete path
				for (int & i = i_s.back(); i < u_s.back()->nxt.size();) {
					u_s.push_back(u_s.back()->nxt[i]);
					jmask_s.push_back(u_s.back()->get_job_mask());
					i++;
					i_s.push_back(0);
					// Double check this!
					is_trivial_s.push_back(is_trivial_s.back() & u_s.back()->get_machine_mask());		// bitwise AND of bitmasks of blocks of operations alonge critiacal path != 0 <=> path is trivial
					goto outer_continue;
				}

				jmask_s.pop_back();
				u_s.pop_back();
				i_s.pop_back();
				is_trivial_s.pop_back();
			}
		}
	}

	return path;										// God bless move semantic
}

bool Graph::is_compatible(Case* cs) {
	int cnt[32];
	memset(cnt, 0, sizeof(int));

	int n = cs->get_job_cnt();
	for (int i = 0; i < n; i++) {
		cnt[cs->get_job(i).get_machines()]++;
	}

	for (int i = 0; i < nodes.size(); i++) {
		int jm = nodes[i]->get_job_mask();
		if (cnt[jm] == 0)
			return false;
		cnt[jm]++;
	}

	for (int i = 0; i < 32; i++)
		if (cnt[i] == 1)
			return false;

	return true;
}

double Graph::early_sched_length(Case& cs) {
	vector<Node*> ts = top_sort();

	for (int i = 0; i < ts.size(); i++)
		ts[i]->start_time = 0.0;

	double duration[32];
	for (int i = 0; i < 32; i++) duration[i] = 0.0;		// redundant but I love initialized data.

	int n = cs.get_job_cnt();
	for (int i = 0; i < n; i++) {
		int mask = cs.get_job(i).get_machines();
		assert(mask >= 0 && mask < 32);
		assert(duration[mask] == 0.0);
		duration[mask] = cs.get_job(i).get_duration();
	}

	double cmax = 0.0;
	for (int i = 0; i < (int)ts.size(); i++) {
		Node *cur = ts[i];
		double cur_duration = duration[cur->job_mask];
		double next_lb = cur->start_time + cur_duration;
		cmax = max(cmax, next_lb);

		for (int j = 0; j < cur->nxt.size(); j++) {
			Node *nxt = cur->nxt[j];
			nxt->start_time = max(nxt->start_time, next_lb);
		}
	}

	return cmax;
}

void Graph::release() {
	if (!_leased)
		delete this;
}

Graph::~Graph()
{
	for (int i = 0; i < nodes.size(); i++)
		delete nodes[i];
}
