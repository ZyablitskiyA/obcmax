#pragma once
#include <vector>
#include <string>
#include "Node.h"

class Case;

// Here shold be logic for deleting transitive edges, listing critical pathes, etc..
class Graph
{
private:
	std::vector<Node*> nodes;
	void dfs_ts(Node *cur, std::vector<Node*> &topsort);
	void set_visited_to(int val);
	Node* find_or_create_node(int job_mask, int machine_mask);

	bool _leased;
public:
	Graph(bool leased = false);
	Graph(std::istream &inp, bool leased = true);		// making leased true by default as it is either initial sched or one of the good schedules list. So should bot be deleted.

	void add_vertex(Node *ptr);
	std::vector<Node*> top_sort();
	std::vector<Node*> get_nodes();
	void dfs(Node* cur);					// Needed for transitive path removal.
	void remove_transitive_edges();
	void draw(std::ostream &);
	std::vector<std::vector<int> > list_complete_paths();

	bool is_compatible(Case* cs);			// Have equal machine sets.
	double early_sched_length(Case& cs);

	void release();
	~Graph();
};

