#include "Node.h"
#include <algorithm>

Node::Node(int job_mask, int machine_mask)
{
	this->job_mask = job_mask;
	this->machine_mask = machine_mask;
	this->visited = 0;
}

void Node::add_machine(int number)
{
	machine_mask |= (1 << number);
}

int Node::get_job_mask()
{
	return this->job_mask;
}

int Node::get_machine_mask()
{
	return this->machine_mask;
}

Node * Node::get_next(int i)
{
	return nxt[i];
}

void Node::delete_edge(int i)
{
	// should never be used.
	this->nxt.erase(nxt.begin() + i);
}

int Node::get_next_cnt()
{
	return (int)nxt.size();
}

Node::~Node()
{
}

void Node::add_edge(Node * to)
{
	// TODO avoid duplicate edges. Maybe done more efficiently
	if (std::find(nxt.begin(), nxt.end(), to) == nxt.end())
		nxt.push_back(to);
}