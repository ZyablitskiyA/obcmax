#pragma once
#include <vector>

// Node represent one block of operations of one job (single or more operations in one block)
// Node is identifiable by pair (job_mask, machine_mask).
class Node
{
	friend class Graph;
private:
	int visited;
	double start_time;					// hack, should not be here.
protected:
	std::vector<Node*> nxt;
	int job_mask;						// Bitmask of job this block corresponds to.
	int machine_mask;					// Bitmask of machines at wich operations of this block are executed.

public:
	void add_edge(Node *to);
	Node(int job_mask, int machine_mask);
	void add_machine(int number);

	int get_job_mask();					// Represent machine set of a job this operation's block belongs to
	int get_machine_mask();				// Represent machine set this operation's block is executed on
	Node* get_next(int i);
	void delete_edge(int i);
	int get_next_cnt();

	~Node();
};
