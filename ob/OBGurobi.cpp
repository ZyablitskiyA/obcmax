#include "OBGurobi.h"
#include <cassert>
#include <algorithm>
#include <string>
#include "Timer.h"
#include "GRBEnvSingleton.h"
#include "globals.h"

using namespace std;

// Load on machine
void OBGurobi::init_li() {
	assert(jobs.size() != 0);
	if (_li.size() != 0)
		return;

	for (int i = 0; i < jobs.size(); i++) {
		double p = jobs[i].get_p();
		const vector<int> &machines = jobs[i].get_machines();
		for (int k = 0; k < machines.size(); k++) {
			int m_no = machines[k] - 1;
			if (m_no >= (int)_li.size())
				_li.resize(m_no + 1, 0.0);
			_li[m_no] += p;
		}
	}

	assert(_li.size() != 0);
}

int OBGurobi::max_machine_number()
{
	init_li();

	return (int)_li.size();
}

int OBGurobi::N(int i)
{
	return (int)jobs[i].get_machines().size();
}

// Slow implementation for now
int OBGurobi::R(int i, int k)
{
	int found = 0;
	const vector<int> & machines = jobs[i].get_machines();
	for (int j = 0; j < machines.size(); j++)
		if (machines[j]-1 == k) found = 1;

	return found;
}

double OBGurobi::P(int i)
{
	return jobs[i].get_p();
}

double OBGurobi::Lmax()
{
	init_li();

	return *std::max_element(_li.begin(), _li.end());
}

double OBGurobi::Li(int i) {
	init_li();
	assert(i < _li.size());
	return _li[i];
}

GRBVar OBGurobi::X(int i, int j, int k)
{
	return x[i][k][j];
}

GRBVar OBGurobi::S(int i, int j, int k)
{
	return s[i][k][j];
}

GRBVar OBGurobi::Q(int i, int k1, int k2)
{
	assert(k1 != k2);
	if (k2 > k1) std::swap(k1, k2);
	return q[i][k1][k2];
}

GRBVar OBGurobi::Z(int k, int i1, int j1, int i2, int j2)
{
	if (i1 < i2) {
		std::swap(i1, i2);
		std::swap(j1, j2);
	}

	return z[k][i1][i2][j1][j2];
}

OBGurobi::OBGurobi() : OBGurobi(105.0) { }

OBGurobi::OBGurobi(double M) : OBSolver(nullptr), model(GRBModel(GRBEnvSingleton::get())), M(M)
{
	if (!Args::VerobseGurobi) {
		model.set(GRB_IntParam_LogToConsole, 0);
	}
	//model.getEnv().set(GRB_DoubleParam_TimeLimit, 600);
}


OBGurobi::~OBGurobi()
{
}

void OBGurobi::set_M(double M)
{
	this->M = M;
}

double OBGurobi::get_M()
{
	return this->M;
}

void OBGurobi::add_job(const job & j)
{
	this->jobs.push_back(j);
}

// Extends one-jobs to li equals 1
void OBGurobi::create_transperant_one_jobs_and_adjust_duration() {
	vector<bool> has_one_job(get_machines_cnt(), false);
	for (int i = 0; i < jobs.size(); i++) {
		const vector<int> &machines = jobs[i].get_machines();
		if (machines.size() == 1) {
			int machine_number = machines[0] - 1;
			assert(!has_one_job[machine_number]);			// only 1 one-job per machine allowed.
			has_one_job[machine_number] = true;
			double li = Li(machine_number);
			double duration = jobs[i].get_p();
			jobs[i].set_p(1.0 - li + duration);
		}
	}
	//*
	for (int i = 0; i < has_one_job.size(); i++) {
		if (!has_one_job[i]) {
			vector<int> param(1, i+1);
			add_job(job(1.0-Li(i), param, true));
		}
	}
	//*/
	_li.resize(0);			// invalidate _li
}

pair<Graph*, double> OBGurobi::optimise(Case & cs)
{
	const double eps = 0.000001;

	// Adding non-zero jobs to solver

	int job_cnt = cs.get_job_cnt();
	for (int i = 0; i < job_cnt; i++) {
		Job j = cs.get_job(i);
		if (j.get_duration() > eps)						// TODO: epsilon
			this->add_job(job(j.get_duration(), j.machines_as_array()));
	}

	create_transperant_one_jobs_and_adjust_duration();

	this->set_M(this->Lmax()*2.0);						// TODO: smth wise

	double cmax = this->optimise();
	
	// Next we build graph for a obtained schedule

	// NOT SURE THAT THE CODE BELOW CORRESPONDS TO THIS COMMENT.
	// Zero-lenth jobs are put to the begining and each job as a block (I belive so we get less complete pathes).
	// Each block of each job is represented as a single vertex in graph.
	// No transitive edges: two nodes connected if they have operations (blocks) on common machine and one goes immediately after another one OR
	// Thes corresponds to two blocks of the same job and one goes exactly after another (wrong, see ascii art below).
	// Now is 01:46 am so please double check all the above.
	// ---aaa-----bbbb----
	// ------aaa--bbbb----
	// -----------bbbb-aaa
	// UPD from fall18. I don remeber anything but guess issue is solved by sched->remove_transitive_edges (was commented out dont remember why).
	//

	// If some path is a sub-sequence of another one, could bouth be complete? No, because then graph is guaranteed to have transitive edges.

	Graph *sched = new Graph;
	vector<vector<std::pair<double, Node*> > > g(get_machines_cnt());		// This arr needed for adding edges that show precedence inside each machine. First element in pair is start time for sorting.

	// Zero length jobs
	for (int i = 0; i < job_cnt; i++) {
		Job j = cs.get_job(i);
		if (j.get_duration() < eps) {					// TODO: epsilon
			int m_mask = j.get_machines();
			Node *cur = new Node(m_mask, m_mask);
			for (int j = 0; (m_mask >> j) > 0; j++) if ((m_mask >> j)%2 == 1) {
				if (g.size() < j + 1)			// this is needed because initially we did not count zero-legth jobs
					g.resize(j + 1);
				g[j].push_back(make_pair(0.0, cur));
			}
			sched->add_vertex(cur);
		}
	}

	// Here only non-zero len jobs
	int m = get_machines_cnt();
	for (int i = 0; i < this->jobs.size(); i++) {
		if (jobs[i].is_transperant())
			continue;

		int Ni = N(i);
		int prev_machine = -1;
		Node *last = nullptr;
		for (int j = 0; j < Ni; j++) {
			for (int k = 0; k < m; k++) {
				if (X(i, j, k).get(GRB_DoubleAttr_X) > 0.9) {				// TODO: why use GRB_DoubleAttr_X for integers?
					if (prev_machine != -1) {
						if (Q(i, prev_machine, k).get(GRB_DoubleAttr_X) > 0.9) {		// Add machine to block.
							last->add_machine(k);							// TODO: add the assert in add_machine that it is added first time
						}
						else {															// Start next block of the i'th that job.
							Node *cur = new Node(jobs[i].get_machines_mask(), 1 << k);
							last->add_edge(cur);
							sched->add_vertex(cur);
							last = cur;
						}
					}
					else {																// Start first block of the i'th that job.
						last = new Node(jobs[i].get_machines_mask(), 1 << k);
						sched->add_vertex(last);
					}
					prev_machine = k;
					double start_time = S(i, j, k).get(GRB_DoubleAttr_X);
					g[k].push_back(make_pair(start_time, last));
				}
			}
		}
	}

	for (int i = 0; i < (int)g.size(); i++) {
		std::sort(g[i].begin(), g[i].end(), [](const pair<double, Node*> &a, const pair<double, Node*> &b) -> bool {
			return a.first < b.first;
		});

		for (int j = 0; j < ((int)g[i].size()) - 1; j++) {
			g[i][j].second->add_edge(g[i][j + 1].second);
		}
	}

	sched->remove_transitive_edges();

	return make_pair(sched, cmax);
}

double OBGurobi::optimise()
{
	int n = get_jobs_cnt();
	int m = get_machines_cnt();

	x.resize(n);
	s.resize(n);
	for (int i = 0; i < n; i++) {
		x[i].resize(m);
		s[i].resize(m);
		for (int k = 0; k < m; k++) {
			int Ni = N(i);
			for (int j = 0; j < Ni; j++) {
				x[i][k].push_back(model.addVar(0.0, 1.0, 0.0, GRB_BINARY, string("x") + to_string(i) + to_string(j) + to_string(k)));
				s[i][k].push_back(model.addVar(0.0, M, 0.0, GRB_CONTINUOUS, string("s") + to_string(i) + to_string(j) + to_string(k)));
			}
		}
	}

	for (int i = 0; i < n; i++) {
		int Ni = N(i);
		for (int k = 0; k < m; k++) {
			GRBLinExpr x_j_sum = 0;
			for (int j = 0; j < Ni; j++) {
				x_j_sum += X(i, j, k);
			}
			model.addConstr(x_j_sum == R(i, k), "x_one_machine");
		}
	}

	for (int i = 0; i < n; i++) {
		int Ni = N(i);
		for (int j = 0; j < Ni; j++) {
			GRBLinExpr x_k_sum = 0;
			for (int k = 0; k < m; k++) {
				x_k_sum += X(i, j, k);
			}
			model.addConstr(x_k_sum == 1, "x_all_machines");
		}
	}
	//model.addConstr(x + 2 * y <= 4 - 3 * z, "");

	q.resize(n);
	for (int i = 0; i < n; i++) {
		q[i].resize(m);
		for (int k1 = 0; k1 < m; k1++) {
			for (int k2 = 0; k2 < k1; k2++) {
				q[i][k1].push_back(model.addVar(0.0, 1.0, 0.0, GRB_BINARY, string("q") + to_string(i) + to_string(k1) + to_string(k2)));
			}
		}
	}

	for (int i = 0; i < n; i++) {
		int Ni = N(i);
		for (int j = 0; j < Ni - 1; j++) {
			for (int k1 = 0; k1 < m; k1++) {
				for (int k2 = 0; k2 < m; k2++) {
					if (k1 == k2) continue;

					GRBLinExpr c = S(i, j + 1, k2) - S(i, j, k1) - P(i)*(1 - Q(i, k1, k2)) + M * (2 - X(i, j, k1) - X(i, j + 1, k2));
					model.addConstr(c >= 0, "one_job_diff_machines");

					// Warning: appears twice?
					GRBLinExpr block = S(i, j + 1, k2) - S(i, j, k1) - M * (1 - Q(i, k1, k2));
					model.addConstr(block <= 0, "one_job_diff_machines_block");
				}
			}
		}
	}

	// Create z variables and store to array
	z.resize(m);
	for (int k = 0; k < m; k++) {
		z[k].resize(n);
		for (int i2 = 0; i2 < n; i2++) {
			z[k][i2].resize(i2);
			for (int i1 = 0; i1 < i2; i1++) {
				int Ni1 = N(i1), Ni2 = N(i2);
				z[k][i2][i1].resize(Ni2);
				for (int j2 = 0; j2 < Ni2; j2++) {
					for (int j1 = 0; j1 < Ni1; j1++) {
						z[k][i2][i1][j2].push_back(model.addVar(0.0, 1.0, 0.0, GRB_BINARY, string("z") + string("_m") + to_string(k) + string("_i") + to_string(i1) + string("_j") + to_string(j1) + string("_i") + to_string(i2) + string("_j") + to_string(j2)));
					}
				}
			}
		}
	}

	// Create constrains that prevent intersection of operations in one machine
	for (int k = 0; k < m; k++) {
		for (int i2 = 0; i2 < n; i2++) {
			for (int i1 = 0; i1 < i2; i1++) {
				int Ni1 = N(i1), Ni2 = N(i2);
				for (int j2 = 0; j2 < Ni2; j2++) {
					for (int j1 = 0; j1 < Ni1; j1++) {
						GRBLinExpr one_machine1 = S(i1, j1, k) - S(i2, j2, k) - P(i2) + M * (2 - X(i1, j1, k) - X(i2, j2, k) + Z(k, i1, j1, i2, j2));
						GRBLinExpr one_machine2 = S(i2, j2, k) - S(i1, j1, k) - P(i1) + M * (3 - X(i1, j1, k) - X(i2, j2, k) - Z(k, i1, j1, i2, j2));
						model.addConstr(one_machine1 >= 0, "One_machine_operations_do_not_intersect");
						model.addConstr(one_machine2 >= 0, "One_machine_operations_do_not_intersect");
					}
				}
			}
		}
	}

	GRBVar Cmax = model.addVar(0.0, M, 0.0, GRB_CONTINUOUS, string("Cmax"));
	for (int k = 0; k < m; k++) {
		for (int i = 0; i < n; i++) {
			int Ni = N(i);
			model.addConstr(S(i, Ni - 1, k) + P(i) <= Cmax);
		}
	}

	model.addConstr(Cmax >= this->Lmax(), "Trying_prevent_searching_after_lmax_achived");

#if 1
	model.setObjective(Cmax+0, GRB_MINIMIZE);
#else
	model.setObjectiveN(Cmax + 0, 0, 1);
	// Minimize number of blocks as secondary goal
	GRBLinExpr minimize_blocks = 0;
	for (int i = 0; i < n; i++) {
		const vector<int> &m = jobs[i].get_machines();
		for (int k1 = 0; k1 < (int)m.size(); k1++)
			for (int k2 = 0; k2 < k1; k2++) {
				minimize_blocks += Q(i, m[k1] - 1, m[k2] - 1);
			}
	}
	model.setObjectiveN(minimize_blocks, 1, 0, +1.0);
	model.set(GRB_DoubleParam_MIPGap, 0.0);			// This is important. Otherwise gurobi stops with not optimal solution.
#endif
	model.update();
	Timer::start(Timer::ILP_SCHED_SOLVING);
	model.optimize();
	Timer::stop(Timer::ILP_SCHED_SOLVING);
	return Cmax.get(GRB_DoubleAttr_X);
}

int OBGurobi::get_jobs_cnt()
{
	return (int)this->jobs.size();
}

int OBGurobi::get_machines_cnt()
{
	return this->max_machine_number();
}

struct Operation {
public:
	Operation(int m, int s, int e, int jb) : machine(m), start_time(s), end_time(e), jb_num(jb) { ; }

	int machine;
	int start_time;
	int end_time;
	int jb_num;

	bool operator < (const Operation& other) const
	{
		if (this->machine < other.machine) return true;
		if (this->machine == other.machine && this->start_time < other.start_time) return true;
		return false;
	}
};

int toint(double d)
{
	return (int)(round(d) + 0.1);
}

void OBGurobi::print_s(ostream & out)
{
	int n = get_jobs_cnt();
	int m = get_machines_cnt();
	for (int k = 0; k < m; k++) {
		for (int i = 0; i < n; i++) {
			int Ni = N(i);
			for (int j = 0; j < Ni; j++) {
				if (X(i, j, k).get(GRB_DoubleAttr_X) > 0.9) {
					double Sijk = S(i, j, k).get(GRB_DoubleAttr_X);
					out << S(i, j, k).get(GRB_StringAttr_VarName) << " len " << P(i) << ": " << Sijk << endl;
				}
			}
		}
	}
}

void OBGurobi::draw_chart(ostream & out)
{
	vector<Operation> g;
	int Emax = 0;

	int n = get_jobs_cnt();
	int m = get_machines_cnt();
	for (int k = 0; k < m; k++) {
		for (int i = 0; i < n; i++) {
			int Ni = N(i);
			for (int j = 0; j < Ni; j++) {
				if (X(i, j, k).get(GRB_DoubleAttr_X) > 0.9) {
					double Sijk = S(i, j, k).get(GRB_DoubleAttr_X);
					int S_int = toint(Sijk);
					int E_int = S_int + toint(P(i));
					g.push_back(Operation(k, S_int, E_int, i));
					Emax = max(Emax, E_int);
				}
			}
		}
	}

	std::sort(g.begin(), g.end());

	vector<string> bitmap;
	string temp;
	temp.resize(Emax, '_');


	for (int i = 0; i < m; i++) {
		bitmap.push_back(temp);
	}

	for (int i = 0; i < g.size(); i++) {
		Operation & op = g[i];
		char pixel = (op.jb_num + '0' < '_') ? (char)(op.jb_num + '0') : (char)(op.jb_num + '0' + 1);	// Skip underscore as it reserved as empty symbol
		for (int j = op.start_time; j < op.end_time; j++)
			bitmap[op.machine][j] = pixel;
	}

	for (int i = 0; i < bitmap.size(); i++) {
		out << i << ' ' << bitmap[i] << endl;
	}
}

void OBGurobi::print_z(ostream & out)
{
	for (int k = 0; k < z.size(); k++) {
		for (int i1 = 0; i1 < z[k].size(); i1++) {
			for (int i2 = 0; i2 < z[k][i1].size(); i2++) {
				for (int j1 = 0; j1 < z[k][i1][i2].size(); j1++) {
					for (int j2 = 0; j2 < z[k][i1][i2][j1].size(); j2++) {
						out << z[k][i1][i2][j1][j2].get(GRB_StringAttr_VarName) << ": " << z[k][i1][i2][j1][j2].get(GRB_DoubleAttr_X) << std::endl;
					}
				}
			}
		}
	}
}

void OBGurobi::print_x(ostream & out)
{
	for (int i = 0; i < x.size(); i++) {
		for (int j = 0; j < x[i].size(); j++) {
			for (int k = 0; k < x[i][j].size(); k++) {
				out << x[i][j][k].get(GRB_StringAttr_VarName) << ": " << x[i][j][k].get(GRB_DoubleAttr_X) << std::endl;
			}
		}
	}
}
