#pragma once
#include "gurobi_c++.h"
#include <string>
#include "job.h"
#include "Case.h"
#include "Graph.h"
#include "OBSolver.h"

class OBGurobi : public OBSolver
{
private:
	GRBModel model;

	double M;

	vector<job> jobs;
	vector<double> _li;
	void init_li();
	int max_machine_number();

	// Number of operations of job i
	int N(int i);
	int R(int i, int k);
	double P(int i);

	vector<vector<vector<GRBVar> > > x;
	vector<vector<vector<GRBVar> > > s;
	vector<vector<vector<GRBVar> > > q;
	vector<vector<vector<vector<vector<GRBVar> > > > > z;

	GRBVar X(int i, int j, int k);
	GRBVar S(int i, int j, int k);
	GRBVar Q(int i, int k1, int k2);
	GRBVar Z(int k, int i1, int j1, int i2, int j2);

	void create_transperant_one_jobs_and_adjust_duration();

public:
	OBGurobi();
	OBGurobi(double M);
	~OBGurobi();

	double Lmax();
	double Li(int i);
	void set_M(double M);
	double get_M();

	void add_job(const job &j);
	virtual pair<Graph*, double> optimise(Case &cs);
	double optimise();
	int get_jobs_cnt();
	int get_machines_cnt();

	void print_s(ostream & out);
	void draw_chart(ostream & out);
	void print_z(ostream & out);
	void print_x(ostream & out);
};

