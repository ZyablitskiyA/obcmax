#include "OBSchedMatcher.h"
#include <fstream>

using namespace std;

vector<OBSchedMatcher::SchedInfo> OBSchedMatcher::_scheds;

void OBSchedMatcher::static_init() {
	ifstream inp("schedule.txt");
	if (!inp.is_open()) {
		cerr << "Failed to open file schedule.txt. Will not use schedule matcher." << endl;
		return;
	}

	load_schedules(inp);
}

void OBSchedMatcher::load_schedules(std::istream& inp) {
	static bool first_call = true;
	assert(first_call);
	first_call = false;

	int n;
	inp >> n;

	for (int i = 0; i < n; i++) {
		_scheds.push_back(new Graph(inp));
	}
}

std::pair<Graph*, double> OBSchedMatcher::optimise(Case &cs) {
	const double eps = 1.0e-7;
	for (int i = 0; i < _scheds.size(); i++) {
		Graph *g = _scheds[i]._sched;
		double cmax = g->early_sched_length(cs);
		//std::cerr << "length for loaded sched " << i+1 << ": " << cmax << std::endl;
		assert(cmax > 1 - eps);
		if (cmax < 1 + eps) {
			_scheds[i]._hitrate++;
			return make_pair(g, cmax);
		}
	}

	return _fallback->optimise(cs);
}

void OBSchedMatcher::print_stat(ostream & out)
{
	if (_scheds.empty())
		return;

	out << "OBSchedMatcher hitrate per schedule:" << endl;
	for (int i = 0; i < (int)_scheds.size(); i++) {
		out << _scheds[i]._hitrate << ' ';
	}
	out << endl;
}
