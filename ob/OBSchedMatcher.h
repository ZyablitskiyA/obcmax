#pragma once

#include "OBSolver.h"
#include <cassert>

class OBSchedMatcher : public OBSolver
{
private:

	// Loaded schedule could have different job set. The idea is to verify the schedule is suitable on first use
	enum Type {
		SUITABLE,
		UNSUITABLE,
		UNKNOWN
	};

	struct SchedInfo {
	public:
		SchedInfo(Graph* sched) : _t(UNKNOWN), _hitrate(0), _sched(sched) {}
		Type _t;
		int _hitrate;
		Graph* const _sched;
	};

	static std::vector<SchedInfo> _scheds;
	static void load_schedules(std::istream& inp);
public:
	static void static_init();

	OBSchedMatcher(OBSolver *fallback) : OBSolver(fallback) { assert(_fallback != nullptr); }

	virtual std::pair<Graph*, double> optimise(Case &cs);
	static void print_stat(std::ostream &out);
};

