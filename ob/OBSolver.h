#pragma once

#include "Case.h"
#include "Graph.h"

class OBSolver {
protected:
	OBSolver * const _fallback;
public:
	OBSolver(OBSolver *fallback) : _fallback(fallback) {}
	virtual std::pair<Graph*, double> optimise(Case &cs) = 0;
};