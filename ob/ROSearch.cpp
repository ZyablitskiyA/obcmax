#include "ROSearch.h"
#include <algorithm>
#include <fstream>
#include <string>
#include <sstream>
#include <cmath>
#include <stack>
#include <vector>
#include "OBGurobi.h"
#include "OBSchedMatcher.h"
#include "BadCaseBuilder.h"
#include <cassert>
#include <cstdlib>
#include <csignal>
#include "Timer.h"
#include "globals.h"

using namespace std;

const double ROSearch::eps = 1.0e-7;

// TODO: add 'main_graph << "}" << endl;' after last log_edge
string main_graph_filename = "rec_tree.txt";
ofstream main_graph(main_graph_filename);

void log_edge(int u, int v, const vector<int> &cp, bool skipped_end = false)
{
	if (!Args::PrintRecGraph)
	  return;

	static bool first_time_here = true;
	if (first_time_here) {
		first_time_here = false;
		main_graph << "digraph G{" << endl;
		main_graph << "	edge[lblstyle = \"above, sloped\"];" << endl;
	}

	main_graph << "// " << cp.size() << endl;
	main_graph << u << " -> " << v << " [label=\"";

	for (int i = 0; i < cp.size(); i++) {
		main_graph << cp[i] << ' ';
	}

	main_graph << "\"];" << endl;

	if (skipped_end) {
		main_graph << v << " [style = filled];" << endl;
	}
}

ostream& prepare_ostream(const string &node_name) {
#if 0
	static ofstream out;
	out.close();
	out.clear();
	out.open(node_name);
	return out;
#else
	static ofstream out("nodes.txt");
	out << endl << node_name << endl;
	return out;
#endif
}

void log_vertex(int u, Case & bad_case, double RO, double lambda, Graph *sched, double cmax)
{
	if (!Args::PrintNodeInfo)
		return;

	string filename(string("node-") + std::to_string(u));
	ostream &out = prepare_ostream(filename);

	if (!out) {
		std::cerr << "Could not open the file: " << filename << std::endl;
	}
	assert(out);
	out << "// RO lambda cmax" << endl;
	out << RO << ' ' << lambda << ' ' << cmax << endl;
	out << "// bad_case" << endl;
	bad_case.print(out);
	out << "// sched" << endl;
	if (sched != nullptr)
		sched->draw(out);
	else out << 0 << endl;
}

bool ROSearch::is_subset(const std::vector<int> &subset, const std::vector<int> &superset)
{
	constexpr int lim = 1 << 5;			// We trying to solve OB5||Cmax
	int cnt[1<<5];

	for (int i = 0; i < lim; i++) {
		cnt[i] = 0;
	}

	bool overflow = false;
	auto fill_cnt = [&](const vector<int> v, bool inc) {
		for (int i = 0; i < (int)v.size(); i++) {
			if (v[i] <= 0 || v[i] >= lim) {
				overflow = true;
				return;
			}

			if (inc) {
				cnt[v[i]]++;
			}
			else {
				cnt[v[i]]--;
			}
		}
	};

	fill_cnt(superset, true);
	fill_cnt(subset, false);
	assert(!overflow);

	for (int i = 0; i < lim; i++) {
		if (cnt[i] < 0) {
			return false;
		}
	}

	return true;
}

bool ROSearch::is_safe_to_skip(const std::vector<int> &crit_path)
{
	assert(complete_path_list_stk.size() == idx_stk.size());

	auto complete_path_list_it = complete_path_list_stk.begin();
	auto idx_it = idx_stk.begin();
	auto limit = --complete_path_list_stk.end();
	while (complete_path_list_it != limit) {
		const vector < vector < int > > &complete_path_list = *complete_path_list_it;
		int cur_idx = *idx_it;

		for (int i = 0; i < cur_idx; i++) {
			if (is_subset(crit_path, complete_path_list[i])) {
				return true;
			}
		}

		complete_path_list_it++;
		idx_it++;
	}

	return false;
}

void ROSearch::rec(vector<vector<int> > &crit_path_stk, Graph *g, int &node_number)
{
	int cur_number = node_number;

	complete_path_list_stk.push_back(g->list_complete_paths());			// move semantics? NRVO?
	// delete g here?

	int depth = (int)complete_path_list_stk.size();
	vector < vector < int > > &complete_path_list = complete_path_list_stk.back();

	/*
	// Descending sort. Idea is to descent to longest paths first to increase chanse of is_safe_to_skip returing true. Not worked out for 3 machines full test (+8 nodes).
	std::sort(complete_path_list.rbegin(), complete_path_list.rend(),
		[] (const vector<int> &a, const vector<int> &b) {
			return a.size() < b.size();
		}
	);
	*/

	idx_stk.push_back(0);
	assert(idx_stk.size() == depth);

	for (int &idx = idx_stk.back(); idx < complete_path_list.size(); idx++) {
		vector < int > & cur_crit_path = complete_path_list[idx];
		int next_number = ++node_number;
		//*
		if (is_safe_to_skip(cur_crit_path)) {
			log_edge(cur_number, next_number, cur_crit_path, true);
			continue;
		}
		//*/
		log_edge(cur_number, next_number, cur_crit_path);

		crit_path_stk.push_back(cur_crit_path);
		BadCaseBuilder bcb;
		bcb.addCritPathes(crit_path_stk);
		Timer::start(Timer::TOTAL_BAD_CASE_BUILDING);
		Case bad_case = bcb.buildCase(cout);
		Timer::stop(Timer::TOTAL_BAD_CASE_BUILDING);
		double lambda = bcb.get_lambda();

		if (RO + eps < lambda) {
			OBGurobi ob_gurobi;
			OBSchedMatcher ob_sched_matcher(&ob_gurobi);
			Timer::start(Timer::TOTAL_SCHED_SOLVING);
			pair<Graph*, double> res = ob_sched_matcher.optimise(bad_case);
			Timer::stop(Timer::TOTAL_SCHED_SOLVING);
			Graph *sched = res.first;
			double cmax = res.second;

			// TODO debug
			// assert(fabs(cmax - sched->early_sched_length(bad_case)) < eps);


			log_vertex(next_number, bad_case, RO, lambda, sched, cmax);
			if (RO + eps < cmax) {
				RO = cmax;
				cout << RO << endl;
				bad_case.print(cout);
				res.first->draw(cout);
				// log the case
			}
			if (RO + eps < lambda) rec(crit_path_stk, sched, node_number);
			sched->release();		// Maybe delete this in callee to reduce mem usage of this var (g).
		}
		else log_vertex(next_number, bad_case, RO, lambda, nullptr, -1);
		crit_path_stk.pop_back();
	}

	complete_path_list_stk.pop_back();
	idx_stk.pop_back();
}

double ROSearch::find_ro(Graph *g) {
	assert(complete_path_list_stk.size() == 0);
	assert(idx_stk.size() == 0);
	int trash = 1;
	vector<vector<int> > trash2;
	rec(trash2, g, trash);
	main_graph << "}" << endl;

	return RO;
}