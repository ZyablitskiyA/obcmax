#pragma once
#include <vector>
#include <list>
#include "Graph.h"

// Main recursive search reside here.
class ROSearch
{
private:
	double RO = 1.0;
	const static double eps;

	// This stack contains all complete paths for each rec node starting from root and up to the current node.
	// stack    |  nodes list | crit path //
	std::list < std::vector < std::vector < int > > > complete_path_list_stk;		// don't use vector because it invalidates references after push_back()
	// This stack is in sync with crit_path_stk. It contains current indexes in the nodes lists.
	std::list < int > idx_stk;

	bool is_subset(const std::vector<int> &subset, const std::vector<int> &superset);
	bool is_safe_to_skip(const std::vector<int> &crit_path);

	void rec(std::vector<std::vector<int> > &crit_path, Graph *g, int &node_number);

public:
	double find_ro(Graph *g);
};

