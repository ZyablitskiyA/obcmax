#include "Timer.h"
#include <cassert>

#define STRINGIFY_(x) #x
#define STRINGIFY(x) STRINGIFY_(x)
#define STRINGIFY_COMMA(x) COMMA(STRINGIFY(x))
#define ZERO(x) 0,
#define MINUS_ONE(x) -1,

const string Timer::measureTimeNames[] = {
	TIMER_REASON_LIST(STRINGIFY_COMMA)
};

clock_t Timer::times[] = {
	TIMER_REASON_LIST(MINUS_ONE)
};

clock_t Timer::total_times[] = {
	TIMER_REASON_LIST(ZERO)
};

//map<int, high_resolution_clock::time_point> timers;

void Timer::start(measureTimeFor what)
{
	assert(times[what] == -1);
	times[what] = clock();
}

void Timer::stop(measureTimeFor what)
{
	assert(times[what] != -1);
	total_times[what] += (clock() - times[what]);
	times[what] = -1;
}

void Timer::print_report(ostream &out)
{
	//clock_t sum = 0;
	//for (int i = 0; i < TIMER_REASON_LIST_CNT; i++) {
		//assert(times[i] == -1);			// All timers were stopped to this moment. Disabled as this called from sighandler.
//		sum += total_times[i];
	//}

	out << "Timer statistics" << endl;
	for (int i = 0; i < TIMER_REASON_LIST_CNT; i++) {
		out << measureTimeNames[i] << ": " << (double)total_times[i] / CLOCKS_PER_SEC << " sec " << ((double)total_times[i] / total_times[TOTAL]) * 100.0 << "%" << endl;
	}
	out << "End of timer statistics." << endl;
}