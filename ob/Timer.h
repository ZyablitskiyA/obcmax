#pragma once
//#include <chrono>
#include <iostream>
#include <ctime>
#include <map>
#include <string>

#define COMMA(X) X,

// Use this list to add smth new
#define TIMER_REASON_LIST(ADAPTER) \
ADAPTER(TOTAL) \
ADAPTER(ILP_SCHED_SOLVING) \
ADAPTER(TOTAL_SCHED_SOLVING) \
ADAPTER(LP_BAD_CASE_BUILDING) \
ADAPTER(TOTAL_BAD_CASE_BUILDING)

using namespace std;
//using namespace std::chrono;

class Timer
{
private:
	static const string measureTimeNames[];
	static clock_t times[];
	static clock_t total_times[];
//	static map<int, high_resolution_clock::time_point> timers;
public:
	enum measureTimeFor {
		TIMER_REASON_LIST(COMMA)
		TIMER_REASON_LIST_CNT
	};

	static void start(measureTimeFor what);
	static void stop(measureTimeFor what);
	static void print_report(ostream &);
};

