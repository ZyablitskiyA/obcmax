#pragma once

// Here will be various flags controlling behaviour of the program
class Args {
public:
	static bool VerobseGurobi;
	static bool PrintNodeInfo;
	static bool PrintRecGraph;
};