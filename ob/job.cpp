#include "job.h"

job::job(double p, const vector<int>& machines, bool transperant) : p(p), machines(machines), _transperant(transperant)
{
}

const vector<int>& job::get_machines()
{
	return machines;
}

int job::get_machines_mask()
{
	int machines_mask = 0;
	for (int i = 0; i < machines.size(); i++) {
		machines_mask |= (1<<(machines[i]-1));		// machines 1-based =(
	}

	return machines_mask;
}

double job::get_p()
{
	return p;
}

void job::set_p(double val) {
	p = val;
}

bool job::is_transperant() {
	return _transperant;
}

job::~job()
{
}
