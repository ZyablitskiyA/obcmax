#pragma once
#include "gurobi_c++.h"
#include <vector>

using namespace std;

class job
{
private:
	double p;
	vector<int> machines;		// Machines numbers where this job present. const by semantic. One-based!!!
	bool _transperant;

public:
	job(double p, const vector<int> &machines, bool transperant=false);
	const vector<int> & get_machines();
	int get_machines_mask();	// 1st machine represented by lsb so Zero-based!!!
	double get_p();
	void set_p(double val);
	bool is_transperant();
	~job();
};

