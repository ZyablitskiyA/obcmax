
#if 0
int main(int   argc,
	char *argv[]) {

}

//#include "gurobi_c++.h"
#include <fstream>
#include <string>
#include <sstream>
#include <cmath>
#include <stack>
#include <vector>
#include "OBGurobi.h"
#include "BadCaseBuilder.h"
#include <cassert>
#include <cstdlib>
#include <csignal>
#include "Timer.h"

using namespace std;

typedef vector<vector<int> > graph;

/* DEPRECATED - same implemented in class Graph
/ This is a dfs written on stack.
/ It saves all complete pathes of graph g to vector path.
/ Path represented as a sequences of vertices in order.
*/
void list_complete_pathes(const graph &g, vector<vector<int> > & path)
{
	vector<int> in_deg(g.size(), 0);

	for (int i = 0; i < g.size(); i++)
		for (int j = 0; j < g[i].size(); j++) {
			in_deg[g[i][j]]++;
		}

	for (int i = 0; i < g.size(); i++) {
		if (in_deg[i] == 0 && g[i].size() > 0) {		// i - start point of complete path(es)
			vector<int> u_s, i_s;
			u_s.push_back(i);
			i_s.push_back(0);
			while (!u_s.empty()) {
			outer_continue:;
				if (g[u_s.back()].size() == 0) path.push_back(u_s);	// save complete path
				for (int & i = i_s.back(); i < g[u_s.back()].size();) {
					u_s.push_back(g[u_s.back()][i]);
					i++;
					i_s.push_back(0);
					goto outer_continue;
				}

				u_s.pop_back();
				i_s.pop_back();
			}
		}
	}
}

double RO = 1.0;
double eps = 1.0e-7;

string main_graph_filename = "rec_tree.txt";
ofstream main_graph(main_graph_filename);

void log_edge(int u, int v, const vector<int> &cp)
{
	static bool first_time_here = true;
	if (first_time_here) {
		first_time_here = false;
		main_graph << "digraph G{" << endl;
		main_graph << "	edge[lblstyle = \"above, sloped\"];" << endl;
	}

	main_graph << "// " << cp.size() << endl;
	main_graph << u << " -> " << v << " [label=\"";

	for (int i = 0; i < cp.size(); i++) {
		main_graph << cp[i] << ' ';
	}

	main_graph << "\"];" << endl;
}

void log_vertex(int u, Case & bad_case, double RO, double lambda, Graph *sched, double cmax)
{
	string filename(string("node-") + std::to_string(u));
	ofstream out(filename);
	if (!out) {
		std::cerr << "Could not open the file: " << filename << std::endl;
	}
	assert(out);
	out << "// RO lambda cmax" << endl;
	out << RO << ' ' << lambda << ' ' << cmax << endl;
	out << "// bad_case" << endl;
	bad_case.print(out);
	out << "// sched" << endl;
	if (sched != nullptr)
		sched->draw(out);
	else out << 0 << endl;
}

// 
void rec(vector<vector<int> > &crit_path, Graph *g, int &node_number)
{
	int cur_number = node_number;
	g->draw(cout);
	vector<vector<int> > path = g->list_complete_pathes();			// move semantics
	//list_complete_pathes(g, path);
	for (int i = 0; i < path.size(); i++) {
		for (int j = 0; j < path[i].size(); j++)
			cerr << path[i][j] << ' ';
		cerr << endl;
	}

	for (int i = 0; i < path.size(); i++) {
		int next_number = ++node_number;
		log_edge(cur_number, next_number, path[i]);
		crit_path.push_back(path[i]);
		BadCaseBuilder bcb;
		bcb.generateConstrains(*g);			// Could g be deleted after this line?
		bcb.addCritPathes(crit_path);
		cout << "Going to build case -----------------" << endl;
		Timer::start(Timer::TOTAL_BAD_CASE_BUILDING);
		Case bad_case = bcb.buildCase(cout);
		Timer::stop(Timer::TOTAL_BAD_CASE_BUILDING);
		bad_case.print(cout);
		double lambda = bcb.get_lambda();		// TODO: is lambda always a makespan of this case-schedule pair? May be calculate makespan separately and add an assert.
		if (RO + eps < lambda) {
			OB ob;
			cout << "SOlve case -----------------" << endl;
			Timer::start(Timer::TOTAL_SCHED_SOLVING);
			pair<Graph*, double> res = ob.optimise(bad_case);
			Timer::stop(Timer::TOTAL_SCHED_SOLVING);
			Graph *sched = res.first;
			double cmax = res.second;
			log_vertex(next_number, bad_case, RO, lambda, sched, cmax);
			if (RO + eps < cmax) {
				RO = cmax;
				cout << "-------------------" << endl;
				cout << RO << endl;
				bad_case.print(cout);
				res.first->draw(cout);
				cout << "-------------------" << endl;
				system("pause");
				// log the case
			}
			if (RO + eps < lambda) rec(crit_path, sched, node_number);
			delete sched;		// Maybe delete this in callee to reduce mem usage of this var (g).
		}
		else log_vertex(next_number, bad_case, RO, lambda, nullptr, -1);
		crit_path.pop_back();
	}
}

void test_graph_building()
{
	//Case cs;
	//cs.add_job(Job(1, 1));
	//cs.add_job(Job(2, 1));
	//cs.add_job(Job(3, 1));
	//cs.add_job(Job(4, 1));
	//cs.add_job(Job(5, 1));
	//cs.add_job(Job(6, 1));
	//OB ob;
	//pair<Graph*, double> res = ob.optimise(cs);
	//ob.draw_chart(cout);
	//cout << "----------------" << endl;
	//res.first->draw(cout);
	//cout << "----------------" << endl;
	//vector<vector<int>> p = res.first->list_complete_pathes();
	//for (int i = 0; i < p.size(); i++) {
	//	for (int j = 0; j < p[i].size(); j++) {
	//		cout << p[i][j] << ' ';
	//	}
	//	cout << endl;
	//}

	//rec(vector<vector<int> >(), res.first);


	Graph *g = new Graph(ifstream("graph.txt"));
	g->draw(cout);
	int trash = 1;
	rec(vector<vector<int> >(), g, trash);

	cout << "RO: " << RO << endl;
	system("pause");
}

void sigintHandler(int sig)
{
	ofstream t("timer.txt");
	Timer::stop(Timer::TOTAL);
	Timer::print_report(t);
	exit(3);
}

int main(int   argc,
     char *argv[])
{
	signal(SIGINT, sigintHandler);
	Timer::start(Timer::TOTAL);
	test_graph_building();
	main_graph << "}" << endl;
	//system((string("dot -Tsvg ") + main_graph_filename + " -o rec_tree.svg").c_str());
	sigintHandler(0);

	// TODO: ISSUE we do not eliminate trivial pathes

	//ifstream cin("input.txt");
	////ifstream cin("test.txt");
	////ofstream cout("output.txt");

	//OB ob(300);
	//int n;
	//cin >> n;
	//cerr << n << endl;
	//
	//string trash;
	//getline(cin, trash); // read last \n 

	//for (int i = 0; i < n; i++) {
	//	string line;
	//	getline(cin, line);
	//	cerr << "Last line: " << line << endl;
	//	//system("pause");
	//	istringstream istream(line);

	//	double p;
	//	vector<int> machines;
	//	istream >> p;

	//	int tmp;
	//	while (istream >> tmp)
	//		machines.push_back(tmp);

	//	ob.add_job(job(p, machines));
	//	cerr << endl;
	//}
	//
	//ob.set_M(ob.Lmax());
	//cerr << "Using M: " << ob.get_M() << endl;
	//cerr << "Lmax: " << ob.Lmax() << endl;

	//try {
	//double cmax = ob.optimise();
	//ob.draw_chart(cout);
	//ofstream vars("vars.txt");
	//ob.draw_chart(vars);
	//ob.print_s(vars);
	//ob.print_x(vars);
	//ob.print_z(vars);
	//}
	//catch (GRBException e) {
	//	cout << "Error code = " << e.getErrorCode() << endl;
	//	cout << e.getMessage() << endl;
	//}
	//catch (...) {
	//	cout << "Exception during optimization" << endl;
	//}
	system("pause");
	return 0;
}

#endif