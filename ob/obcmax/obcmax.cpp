//#include "gurobi_c++.h"
#include <fstream>
#include <string>
#include <sstream>
#include <cmath>
#include <stack>
#include <vector>
#include "../OBGurobi.h"
#include "../BadCaseBuilder.h"
#include <cassert>
#include <cstdlib>
#include <csignal>
#include "../Timer.h"
#include "../ROSearch.h"
#include "../GRBEnvSingleton.h"
#include "../OBSchedMatcher.h"
#include "../globals.h"

using namespace std;

typedef vector<vector<int> > graph;

void test_graph_building()
{

	ifstream graph("graph.txt");
	if (!graph.is_open()) {
		cout << "Failed to open file graph.txt" << endl;
		return;
	}

	Graph *g = new Graph(graph);
	BadCaseBuilder::generateStaticConstrains(*g, graph);

	OBSchedMatcher::static_init();

	g->draw(cout);
	ROSearch ro_search;
	ro_search.find_ro(g);
}

void sigintHandler(int sig)
{
	ofstream t("timer.txt");
	Timer::stop(Timer::TOTAL);
	Timer::print_report(t);
	OBSchedMatcher::print_stat(t);
	exit(sig);					// otherwise execution continued after signal is handled.
}

bool process_args(int   argc,
	char *argv[]) {
	if (argc < 2)
		return true;
	if (strncmp(argv[1], "-v", 2) == 0) {
		Args::VerobseGurobi = true;
		Args::PrintNodeInfo = true;
		Args::PrintRecGraph = true;
	}
	return true;
}

int main(int   argc,
	char *argv[])
{
	process_args(argc, argv);
	signal(SIGINT, sigintHandler);
	Timer::start(Timer::TOTAL);
	test_graph_building();
	//main_graph << "}" << endl;
	//system((string("dot -Tsvg ") + main_graph_filename + " -o rec_tree.svg").c_str());
	sigintHandler(0);

	// TODO: ISSUE we do not eliminate trivial pathes

	return 0;
}
